<?php
/*
	Tablice to zmienne, które mogą posiadać więcej niż jedną wartość. Np.
	$imiona = ['Justyna','Antoni','Klaudyna','Emil'];
	
	utworzy nam zmienną $imiona, która będzie zawierała 4 imiona. Jeżeli chcemy 
	odwołać się do imienia Klaudyna:
	
	echo $imiona[2]; <- to spowoduje wyświetlenie tego imienia
	
	Oznacza to, że tablice numerowane są od 0 (w powyższym przypadku mamy odpowiednio
	indeksy 0,1,2,3).
	
	Gdybyśmy odwołali się w taki sposób:
	
	echo $imiona[5];
	
	to spowodujemy błąd/ostrzeżebnie - otrzymamy komunikat, że zmienna nie została
	zainicjowana
	
	Tablice w PHP mogą mieć tzw. własne indeksy:
	
	$imiona= [12 => 'Konrad', -5 => 'Damian', 'specjalne' => 'Malina']
	
	Powyżej mamy indeksy: 12, -5 oraz 'specjalne'.
	
	wywołania:
	echo $imiona['specjalne'] //wyświetli imię Malina
	echo $imiona[-5] //wyświetli imię Damian
*/
	if (!isset($_POST['imie'])) {
		echo 'Błędne dane, nic nie zostało dodane do bazy!';
		return;
	}
	$sqlLink = mysqli_connect('127.0.0.1','root','','zadanie1_2019');
	mysqli_set_charset($sqlLink, 'utf8');
	
	mysqli_query($sqlLink, "INSERT INTO `osoby` (`imie`,`nazwisko`,`tresc`) VALUES ('{$_POST['imie']}','{$_POST['nazwisko']}','{$_POST['tresc']}');");
	mysqli_close($sqlLink);
	echo "Dodano nową wiadomość!";